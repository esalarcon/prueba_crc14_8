# Prueba CRC-14

Prueba de rendimiento del CRC-14 con polinomio 0x21E8 utilizando la aproximación multibit de [Sarwate](https://en.wikipedia.org/wiki/Computation_of_cyclic_redundancy_checks).<br>
Se corrió en un Cortex-M3 [STM32F103C8](https://www.st.com/en/microcontrollers-microprocessors/stm32f103c8.html) a 72Mhz de frecuencia de reloj.<br> 
Usando el la función de conteo de ciclos del timer [DWT](https://developer.arm.com/documentation/ddi0337/h/BIIFBHIF) del Cortex-M3 se midió que para una **trama de 1019 bytes tarda 50120 ciclos de reloj** en calcular el CRC.
