/*
 * crc14_d8.h
 *
 *  Created on: 19 may. 2021
 *      Author: esala
 */

#ifndef INTERFAZ_INC_CRC14_D8_H_
#define INTERFAZ_INC_CRC14_D8_H_
	#include <stdint.h>
	int trama_sia_crc_ok(uint8_t *b, uint32_t length);
	void crc_append(uint8_t *b, uint32_t length);

#endif /* INTERFAZ_INC_CRC14_D8_H_ */
